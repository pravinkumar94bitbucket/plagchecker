import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CKEditorModule } from 'ng2-ckeditor';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { MyplagComponent } from './myplag/myplag.component';


@NgModule({
  declarations: [
    AppComponent,
    MyplagComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CKEditorModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

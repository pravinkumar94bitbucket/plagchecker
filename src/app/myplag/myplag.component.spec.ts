import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyplagComponent } from './myplag.component';

describe('MyplagComponent', () => {
  let component: MyplagComponent;
  let fixture: ComponentFixture<MyplagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyplagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyplagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

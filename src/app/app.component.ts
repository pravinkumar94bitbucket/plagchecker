import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { RequestOptions } from '@angular/http/src/base_request_options';
import { Response, Http, Headers } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})

export class AppComponent {

  private apiURL = 'https://www.prepostseo.com/apis';

  results: any = {};

  constructor(private http: Http) {
    console.log('Hello World');
    this.getData();
    this.getImages();
  }

  getData() {
    let headers = new Headers();
    headers.set('key','46ef6b680db78daa4d6c684cc3557261');
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', '*/*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization','pravin.kumar@wizquest.io');
    // headers.append('X-Api-Key', '46ef6b680db78daa4d6c684cc3557261');
    headers.append('Access-Control-Allow-Headers', "X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding");
    return this.http.post(this.apiURL, { headers })
      .map(res => { 
        this.results = res;
        console.log(res);
        res.json();});
  }

  getImages() {
    this.getData().subscribe(data => {
      console.log(data);
    });
  }
}
